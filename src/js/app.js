import $ from 'jquery';
import 'slick-carousel';


// $(document).ready(function() {
//   console.log('test');
// });


$( '.service__tab' ).hover(
  function() {
    var index = $(this).index();
    var $bgrds = $(this).closest('.service').find('.service__bg img');
    var $target = $bgrds.eq(index);

    $bgrds.fadeOut(200);
    $target.fadeIn(200);
    
  }, function() {
   
  }
);


function aboutScroll() {
  var $whiteScreen = $('.js-white-screen'),
    whiteScreenPosition = $whiteScreen.offset(),
    topScroll = window.pageYOffset;

  if($whiteScreen.length && topScroll >= whiteScreenPosition.top) {
    $('body').attr('bg-menu', 'white');
  } else {
    $('body').attr('bg-menu', '');
  }
  
}


aboutScroll();


$(window).scroll(function() {  
  aboutScroll();
});



var aForResize;
$(window).resize(function() {
  clearTimeout(aForResize);
  aForResize = setTimeout(function() {
    aboutScroll();

  },200);
});
